#!/bin/bash
# This script will add current files to remote repo
# and commet them directly

set -xe

usage() {
  echo "Usage: ${0} [commit name]" >&2
  echo '     Add & Commit current change to origin remote branch' >&2
  echo '     Insure you are current working git directory' >&2
  exit 1
}

if [[ ! -d .git ]]
then
    echo "Their is no current git working directory" >&2
    usage
fi    

if [[ "${#}" -ne 1 ]]
then
   echo 'one argument needed as commit msg "feature/add new.."' >&2
   usage
fi

RED='\033[0;31m' # red
LRD='\033[1;31m' # light red
LGR='\033[1;32m' # light green
YEL='\033[1;33m' # yellow
NC='\033[0m'     # no color

echo "### Staging changes ###"
status=$(git diff)
echo -e "your staged files: ${LGR}${status}${NC}"

git add . --all

echo "### Commiting changes ###"
msg=${1}
echo -e "your commit msg is: ${YEL}${msg}${NC}"

git commit -m "${msg}"

echo "### Pushing changes to current branch ###"
branch=$(git branch --show-current)
echo -e "${LRD}${branch}${NC}"

if [[ "${branch}" = 'master' ]]
then
    git push
else    
    git push origin ${branch}
fi    
#!/bin/bash
# This script will initilize current directory
# to remote repoistoy
set -x
usage() {
  echo "Usage: ${0} [remote repo] [commit name]" >&2
  echo '  Add Current directory to remote repo' >&2
  echo '  and commit current changes' >&2
  echo '  remote repo format = git@github.com:User/UserRepo.git' >&2
  exit 1
}

ARGS=2

if [[ "${#}" -ne ${ARGS} ]]
then
   echo 'two argument needed as commit name' >&2
   usage
fi

REPO=${1}
COMMIT=${2}

git init
git remote add origin "${REPO}"
git remote -v
BRANCH=$(git rev-parse --abbrev-ref HEAD)
git pull origin "${BRANCH}"
git add . --all
git commit -am "${COMMIT}"
git push -f origin "${BRANCH}"


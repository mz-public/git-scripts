#!/bin/bash
# This script clear local cache for git ignore
# with option to revert last commit

git pull
git rm -r --cached .
git add .
git commit -am 'git cache cleared'
git push